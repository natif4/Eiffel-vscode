import { CompilerLauncher } from './compilerLauncher';

/**
 * Class for the compiling of the project
 */
export class ProjectCompiler extends CompilerLauncher {

    /**
     * Constructor
     * 
     * @param ecbPath The path of the eiffel compiler
     * @param cwd The current working directory
     * @param projectFileName The project file name
     */
    constructor(ecbPath: string, cwd: string, projectFileName: string) {
        super("Eiffel compiler", ecbPath, cwd, projectFileName);
    }

    /**
     * Compile and run the project
     * 
     * Start the progress bar process, compile the project,
     * end the progress bar process and then run the project.
     */
    public async compileAndRun(): Promise<void> {

        try {
            this._extensionOutputChannel.show();
            this._extensionOutputChannel.appendLine("Compile and run started...");
    
            await this._startProgressBarProcess(100, "Compile and run started ");
    
            //Compile the project
            await this._executeEcbCommandAsync("-batch -stop -loop", ["I", "F", "Y", "W", "Q"]);
    
            //Tells the progress bar process that the job is done by 
            //setting the "_progressionIndex" equal to the "maxProgressIndex" value.
            this._progessionIndex = 100;
    
            this._extensionOutputChannel.appendLine("Project starting...");
    
            //Run the project
            await this._executeEcbCommandAsync("-batch -stop -loop", ["I", "R", "Q"]);
        } catch (error: any) {
            console.log(`Error while compiling the project. Details : ${error}`);
            this._extensionOutputChannel.appendLine(`Error while initializing the project. Details : ${error}`);
            throw new Error(error);
        }
    }
}