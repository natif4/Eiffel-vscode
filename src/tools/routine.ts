/**
 * Class that represents a routine from a class of the Eiffel project
 */
export class Routine {
	name: string = "";
	arguments: string[] = [];
	snippetString: string = "";
	return: string = "";
}