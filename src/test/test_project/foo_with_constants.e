note
	description: "foo class"
	date: "$Date$"
	revision: "$Revision$"

class
	FOO_WITH_CONSTANTS

inherit
	ARGUMENTS_32

create
	make

feature {NONE} -- Initialization

	make
		do
			print ("Foo with constants is working!%N")
		end

feature -- Attributes

    foo_constant_string: STRING = "This is foo's constant!"

    foo_constant_universe_response: INTEGER = 42

end
