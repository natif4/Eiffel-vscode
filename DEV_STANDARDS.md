# Programming language standards used for developpement

*TypeScript* is used to developp this extension. The code standards are based on [the official *Airbnb* standards ](https://github.com/airbnb/javascript)and imposed through *ESLint* in *Visual Studio Code*, but here is a brief list of what we expect from you if you want to contribute to this extension :

- Each line of code should not go beyond **120 characters long**;
- Use ```let``` for variables and ```const``` for constants. NEVER USE ```var```;
- Always add a ";" at the end of a method call or an attribute declaration;
- Each indentation is **4 spaces**;
- All control structures (```routines```, ```if```, ```for```, ```while```, etc.) must be delimited by braces (```{``` and ```}```);
- All keywords and operators (```if```, ```for```, ```+```, ```=```, etc.) must be surrounded by space characters;
- Use meaningful names for all identifiers (classes, methods, attributes, etc.)à
- Use the **camelCase** for objects, methods and instances;
- Use **PascalCase** for constructors and classes;
- Use the ```private _myPrivateFoo``` way to declare **private** or **protected** identifiers;
- For lambdas functions, use the following syntax ```() => {}``` rather than ```function () {}```;
- Methods should not go beyond **45 lines long**.
- Ideally, everything should be documented, although it's not mandatory. On the other hand, short documentation **must** precede any identifier that lacks precision despite its meaningful name (for example, a side effect of a method, specifying an exception, etc.);